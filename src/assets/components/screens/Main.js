// ddddd //

import {
  Alert,
  Button,
  StyleSheet,
  Text,
  View,
  Linking,
  ToastAndroid,
  Platform,
} from 'react-native';
import React, {useEffect} from 'react';
import notifee, {EventType, AndroidImportance} from '@notifee/react-native';
import messaging from '@react-native-firebase/messaging';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

const Main = ({navigation}) => {
  // naviagtion//

  useEffect(() => {
    requestPermission();
    getInitialNotification();
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      console.log('remotemessage', JSON.stringify(remoteMessage));
      DisplayNotification(remoteMessage);

      // Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
    });
  }, []);

  const requestPermission = async () => {
    // Register the device with FCM

    await messaging().registerDeviceForRemoteMessages();

    // Get the token

    const token = await messaging().getToken();
    console.log(token);
    const authStatus = await messaging().requestPermission();
  };

  const getInitialNotification = async () => {
    const message = await messaging().getInitialNotification();
    if (message != null) {
      navigation.navigate('Landing');
    }
    console.log(message.data, 'initial notification');
  };

  async function DisplayNotification(remoteMessage) {
    // Request permissions (required for iOS)

    await notifee.requestPermission();

    // Create a channel (required for Android)

    const channelId = await notifee.createChannel({
      id: 'important',
      name: 'Important Notifications',
      importance: AndroidImportance.HIGH,
      vibration: true,
      sound: 'default',
    });

    const handleNotificationPress = notification => {
      console.log(notification, 'kitty');
    };

    // Display a notification

    await notifee.displayNotification({
      title: remoteMessage.notification.title,
      body: remoteMessage.notification.body,
      android: {
        channelId,
        importance: AndroidImportance.HIGH,
        smallIcon: 'ic_launcher', // optional, defaults to 'ic_launcher'.
        // pressAction is needed if you want the notification to open the app when pressed
        pressAction: {
          id: 'default',
        },
      },
      onNotificationOpenedApp: handleNotificationPress(),
    });
  }

  async function localDisplayNotification() {
    // Request permissions (required for iOS)
    await notifee.requestPermission();

    // Create a channel (required for Android)
    const channelId = await notifee.createChannel({
      id: 'default',
      name: 'Default Channel',
      importance: AndroidImportance.HIGH,
    });

    // Display a notification
    await notifee.displayNotification({
      title:
        '<p style="color: #4caf50;"><b>Styled HTMLTitle</span></p></b></p> &#128576;',
      subtitle: '&#129395;',
      body: 'The <p style="text-decoration: line-through">body can</p> also be <p style="color: #ffffff; background-color: #9c27b0"><i>styled too</i></p> &#127881;!',
      android: {
        importance: AndroidImportance.HIGH,
        channelId,
        color: '#4caf50',
        actions: [
          {
            title: '<b>Dance</b> &#128111;',
            pressAction: {id: 'dance'},
          },
          {
            title: '<p style="color: #f44336;"><b>Cry</b> &#128557;</p>',
            pressAction: {id: 'cry'},
          },
        ],
      },
    });
  }

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Button
        title="Display Notification"
        onPress={() => localDisplayNotification()}
      />
    </View>
  );
};

export default Main;

const styles = StyleSheet.create({});
